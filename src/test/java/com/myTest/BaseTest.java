package com.myTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import com.myAbstract.BasePage;
import com.myAbstract.Page;

//import com.myAbstract.BasePage;
//import com.myAbstract.Page;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseTest {
	
	WebDriver driver;
	public Page page;
	
	@BeforeTest
	@Parameters(value = {"browser"})
	public void setUp(String browser) {
		if(browser.equalsIgnoreCase("chrome")) {
			WebDriverManager.chromedriver().version("86").setup();
			driver = new ChromeDriver();
		}
		else if(browser.equalsIgnoreCase("firefox")) {
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
		}
		
		driver.get("http://demo.guru99.com/");
		page = new BasePage(driver);
	}
	
//	@AfterTest
//	public void afterTest() {
//		driver.quit();
//	}

}
