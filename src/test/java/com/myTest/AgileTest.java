package com.myTest;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.myPages.AgilePage;
import com.myPages.LogInPage;
import com.myPages.RegisterPage;

public class AgileTest extends BaseTest{
	private RegisterPage res = null;
	
	@Test(priority =2)
	public void doLogin1() {
		LogInPage loginPage = res.gotoAgileProject();
		AgilePage agile = loginPage.doLoginAgile();
		
		String title = agile.getTitleAgilePage();
		Assert.assertEquals(title, "GTPL Bank Manager HomePage", "Verify Home Page opened");
	}
}